% Tudor Berariu (C) 2015 <tudor.berariu@gmail.com>

% Povestea (inspirată de Știrile de la Ora 5)

% În liniștitul nostru oraș s-a produs o crimă. Un individ a pătruns
% în casa unui bătrân și l-a ucis. Cadravrul a fost ascuns de către
% criminal și nu este de găsit. Este un caz complicat, dar doi
% detectivi, A și B, fac cercetări și au deja o listă de
% suspecți. Știu despre fiecare dintre aceștia ce mașină are și care
% este arma lui preferată.

% Pentru a rezolva cazul trebuie să afle cu ce armă a fost ucisă
% victima și cu ce mașină a fugit criminalul. Din fericire, dacă se
% poate vorbi despre așa ceva în cazul unei crime îngrozitoare, nu
% există doi suspecți care să aibă aceeași mașină și aceeași armă.

% Cei doi detectivi se întâlnesc la secție. A s-a întâlnit cu un
% martor și a aflat cu ce mașină a fugit criminalul. Între timp, B a
% găsit arma crimei. Cei doi se întâlnesc la secție, unde au următorul
% dialog pe care tu îl asculți indiscret.

% Det. A: Știu că nu știi cine-i criminalul. Nici eu nu știu.
% Det. B: Încă nu știu cine este.
% Det. A: Nici eu nu știu încă cine este criminalul.
% Det. B: Acum mi-am dat seama.
% Det. A: Mi-am dat seama și eu.

% Cine este criminalul?

% ----------------------------------------------------------------------------
% Mașini

conduce(aurel, ford).
conduce(bogdan, bmw).
conduce(cosmin, bmw).
conduce(daniel, ford).
conduce(eugen, bmw).
conduce(florin, dacia).
conduce(george, fiat).
conduce(horia, audi).
conduce(irina, dacia).
conduce(jean, fiat).
conduce(kiki, audi).
conduce(laura, seat).
conduce(marian, mercedes).
conduce(nicodim, opel).
conduce(ovidiu, honda).
conduce(paul, honda).

% Arme

inarmat(aurel, sabie).
inarmat(bogdan, pistol).
inarmat(cosmin, arbaleta).
inarmat(daniel, grenada).
inarmat(eugen, grenada).
inarmat(florin, sabie).
inarmat(george, pistol).
inarmat(horia, arbaleta).
inarmat(irina, pusca).
inarmat(jean, cutit).
inarmat(kiki, prastie).
inarmat(laura, pusca).
inarmat(marian, cutit).
inarmat(nicodim, prastie).
inarmat(ovidiu, maceta).
inarmat(paul, mitraliera).

% ----------------------------------------------------------------------------
% 1. (0.5p) Scrieți un predicat suspect(Nume:Masina:Arma) care să fie
% adevărat pentru fiecare suspect al problemei noastre.

suspect(Nume:Masina:Arma):- conduce(Nume, Masina),  inarmat(Nume, Arma).

% ----------------------------------------------------------------------------
% 2. (0.5p) Scrieți un predicat au_bmw(ListaNume) care să fie adevărat
% atunci când ListaNume este lista cu numele tuturor celor care au
% bmw.  Folosiți setof/1.

au_bmw(ListaNume):- setof(Nume, conduce(Nume, bmw), ListaNume).

% ----------------------------------------------------------------------------
% 3. (0.5p) Scrieți un predicat au_marca(Marca, ListaNume) care să
% fie adevărat atunci când ListaNume este lista cu numele tuturor
% celor care au masina de tipul Marca. Folosiți setof/1.

au_marca(Marca, ListaNume):- setof(Nume, conduce(Nume, Marca), ListaNume).

% ----------------------------------------------------------------------------
% 4. (0.5p) Scrieți un predicat arme_bmw(ListaArme) care să fie adevărat
% atunci când ListaArme reprezinta multimea tuturor armelor detinute
% de conducatori de bmw.  Folosiți setof/3.

arme_bmw(ListaArme):- setof(Arma, Nume^(conduce(Nume, bmw), (inarmat(Nume, Arma))), ListaArme).

% ----------------------------------------------------------------------------
% 5. (0.5p) Scrieți un predicat arme_marca(Marca, ListaArme) care să
% fie adevărat atunci când ListaArme reprezinta multimea tuturor
% armelor detinute de conducatori de masini tipul Marca.  Folosiți
% setof/3.

arme_marca(Marca, ListaArme):- setof(Arma, Nume^(conduce(Nume, Marca), inarmat(Nume, Arma)), ListaArme).

% ----------------------------------------------------------------------------
% 6. (0.5pp) Scrie un predicat marci_arma_unica/1 care să afișeze lista
% mașinilor pentru care lista armelor pe care le dețin conducătorii
% unei mărci conține un singur element. Hint: folosiți-vă de
% rezolvarea exercițiului 5.  Nu folosiți length/2.

marci_arma_unica(Marci):- findall(M, arme_marca(M, [_]), Marci).

% ----------------------------------------------------------------------------
% ----------------------------------------------------------------------------

% Să revenim la secție de poliție unde tu tragi cu urechea la dialogul
% dintre cei doi detetivi.  Să analizăm prima replică.

% Detectiv A : Știam că nu știi cine-i criminalul.

% Ce înseamnă asta? Detectivul A știe mașina cu care a fugit
% suspectul. Această marcă de mașină este condusă de mai mulți
% suspecți care mânuiesc diferite arme. Dacă măcar una dintre aceste
% arme ar fi aparținut doar unui singur suspect, atunci Detectivul B
% ar fi putut ști care este soluția acestui caz. Deci, ce soluții
% eliminăm?

% Să luăm un exemplu.  Dacă Detectivul A ar fi aflat că adevăratul
% criminal a condus o Honda, atunci ar fi existat două soluții
% posibile: ovidiu - honda - maceta și ovidiu - paul - mitraliera. Dar
% cum nu există decât un singur individ care are macetă (ovidiu),
% Detectivul A nu ar fi putut afirma că Detectivul B nu poate ști cine
% este criminalul. De aceea, honda nu poate fi mașina criminalului.

% ----------------------------------------------------------------------------
% 7. (2p) Scrie un predicat suspect1/1 care este adevărat doar pentru
% numele suspectilor care respectă condiția impusă de replica
% Detectivului A.


armaa_u(Masina):- suspect(_:Masina:Arma),findall(Nume, suspect(Nume:_:Arma),[_]).

suspect1(Nume:Masina:Arma):- suspect(Nume:Masina:Arma), \+armaa_u(Masina).

% ----------------------------------------------------------------------------
% Să trecem la partea a doua a primei replici.
%
% Detectivul A: Nici eu nu știu!
%
% 8. (2p) Scrie un predicat suspect2/1 care este adevărat doar pentru
% numele suspecților care respectă condiția impusă de replica
% Detectivului A.
%
% Atenție: informația ce trebuie filtrată acum este cea care
% corespunde primei replici.

suspect2(Nume:Masina:Arma):- suspect1(Nume:Masina:Arma), \+ findall(Numee, suspect(Numee:Masina:_),[_]) .

% ----------------------------------------------------------------------------
% A treia replică:
%
% Detectivul B: Nici eu nu știu!
%
% 9. (2p) Scrie un predicat suspect3/1 care este adevărat doar pentru
% numele suspecților care respectă condiția impusă de replica
% Detectivului B.
%
% Atenție: informația ce trebuie filtrată acum este cea care
% corespunde primelor două replici.

suspect3(Nume:Masina:Arma):- suspect2(Nume:Masina:Arma), \+ findall(Numee, suspect2(Numee:_:Arma), [_]).

% ----------------------------------------------------------------------------
% A patra replică:
%
% Detectivul A: Eu tot nu știu!
%
% 10. (2p) Scrue un predicat suspect4/1 care este adevărat doar pentru
% numele suspecților care respectă condiția impusă de replica
% Detectivului A.
%
% Atenție: informația ce trebuie filtrată acum este cea care
% corespunde primelor trei replici.

suspect4(Nume:Masina:Arma):- suspect3(Nume:Masina:Arma), \+ findall(Numee, suspect3(Numee:Masina:_),[_]).

% ----------------------------------------------------------------------------
% A cincea replică:
%
% Detectivul B: Eu am aflat!
%
% 11. (2p) Scrue un predicat suspect5/1 care este adevărat doar pentru
% numele suspecților care respectă condiția impusă de replica
% Detectivului B.
%
% Atenție: informația ce trebuie filtrată acum este cea care
% corespunde primelor patru replici.

suspect5(Nume:Masina:Arma):- suspect4(Nume:Masina:Arma), findall(Numee, suspect4(Numee:_:Arma), [_]).

% ----------------------------------------------------------------------------
% A șasea replică:
%
% Detectivul A: Și eu am aflat!
%
% 12. (2p) Scrie un predicat suspect6/1 care este adevărat doar pentru
% numele suspecților care respectă condiția impusă de replica
% Detectivului A.
%
% Atenție: informația ce trebuie filtrată acum este cea care
% corespunde primelor cinci replici.

suspect6(Nume:Masina:Arma):- suspect5(Nume:Masina:Arma), findall(Numee, suspect5(Numee:Masina:_), [_]). .

% ------------------------------------------------------------------------------
% ------------------------------------------------------------------------------

success_message(P, I):-
    format('[~2f] Bravo, exercițiul ~d este rezolvat corect!~n', [P,I]).

fail_message(I):-
    format('[0.00] Exercițiul ~w nu este rezolvat corect.~n', [I]).

test_1:-
    setof(Nume:Masina:Arma, suspect(Nume:Masina:Arma), All),
    maplist(==, All,
            [aurel:ford:sabie, bogdan:bmw:pistol,
             cosmin:bmw:arbaleta, daniel:ford:grenada, eugen:bmw:grenada,
             florin:dacia:sabie, george:fiat:pistol, horia:audi:arbaleta,
             irina:dacia:pusca, jean:fiat:cutit, kiki:audi:prastie,
             laura:seat:pusca, marian:mercedes:cutit, nicodim:opel:prastie,
             ovidiu:honda:maceta, paul:honda:mitraliera]),
    !,
    success_message(0.75, 1).

test_1:-
    fail_message(1).

test_2:-
    au_bmw(ListaNume), ListaNume == [bogdan, cosmin, eugen],
    !,
    success_message(0.75, 2).

test_2:-
    fail_message(2).

test_3:-
    au_marca(bmw, BMW), BMW == [bogdan, cosmin, eugen],
    au_marca(dacia, Dacia), Dacia == [florin, irina],
    au_marca(seat, Seat), Seat == [laura],
    !,
    success_message(0.75, 3).

test_3:-
    fail_message(3).

test_4:-
    arme_bmw(ListaArme), ListaArme == [arbaleta, grenada, pistol],
    !,
    success_message(0.75, 4).

test_4:-
    fail_message(4).

test_5:-
    arme_marca(bmw, BMW), BMW == [arbaleta, grenada, pistol],
    arme_marca(dacia, Dacia), Dacia == [pusca, sabie],
    arme_marca(seat, Seat), Seat == [pusca],
    !,
    success_message(1.0, 5).

test_5:-
    fail_message(5).

test_6:-
    marci_arma_unica(Marci),
    msort(Marci, Sorted),
    Sorted == [mercedes, opel, seat],
    !,
    success_message(1, 6).

test_6:-
    fail_message(6).

test_7:-
    setof(Nume:Masina:Arma, suspect1(Nume:Masina:Arma), Lista),
    Lista = [aurel:ford:sabie,bogdan:bmw:pistol,cosmin:bmw:arbaleta,
             daniel:ford:grenada,eugen:bmw:grenada,florin:dacia:sabie,
             george:fiat:pistol,horia:audi:arbaleta,irina:dacia:pusca,
             jean:fiat:cutit,kiki:audi:prastie,laura:seat:pusca,
             marian:mercedes:cutit,nicodim:opel:prastie],
    !,
    success_message(2, 7).

test_7:-
    fail_message(7).

test_8:-
    setof(Nume:Masina:Arma, suspect2(Nume:Masina:Arma), Lista),
    Lista == [aurel:ford:sabie,bogdan:bmw:pistol,cosmin:bmw:arbaleta,
              daniel:ford:grenada,eugen:bmw:grenada,florin:dacia:sabie,
              george:fiat:pistol,horia:audi:arbaleta,irina:dacia:pusca,
              jean:fiat:cutit,kiki:audi:prastie],
    !,
    success_message(2, 8).

test_8:-
    fail_message(8).

test_9:-
    setof(Nume:Masina:Arma, suspect3(Nume:Masina:Arma), Lista),
    Lista == [aurel:ford:sabie,bogdan:bmw:pistol,cosmin:bmw:arbaleta,
              daniel:ford:grenada,eugen:bmw:grenada,florin:dacia:sabie,
              george:fiat:pistol,horia:audi:arbaleta],
    !,
    success_message(1.5, 9).

test_9:-
    fail_message(9).

test_10:-
    setof(Nume:Masina:Arma, suspect4(Nume:Masina:Arma), Lista),
    Lista == [aurel:ford:sabie,bogdan:bmw:pistol,cosmin:bmw:arbaleta,
              daniel:ford:grenada,eugen:bmw:grenada],
    !,
    success_message(1.5, 10).

test_10:-
    fail_message(10).

test_11:-
    setof(Nume:Masina:Arma, suspect5(Nume:Masina:Arma), Lista),
    Lista = [aurel:ford:sabie,bogdan:bmw:pistol,cosmin:bmw:arbaleta],
    !,
    success_message(1.5, 11).

test_11:-
    fail_message(11).

test_12:-
    setof(Nume:Masina:Arma, suspect6(Nume:Masina:Arma), Lista),
    Lista = [aurel:ford:sabie],
    !,
    success_message(1.5, 12).

test_12:-
    fail_message(12).


% ------------------------------------------------------------------------------

:-
    nl,
    test_1,
    test_2,
    test_3,
    test_4,
    test_5,
    test_6,
    test_7,
    test_8,
    test_9,
    test_10,
    test_11,
    test_12.
