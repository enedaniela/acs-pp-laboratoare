module Graph where

import Data.List

{-
        Urmatoarele exercitii vor avea drept scop implementarea unei mini-librarii
    pentru grafuri ORIENTATE.
        Dupa cum stiti, exista mai multe modalitati de reprezentare a unui graf.
    Libraria noastra va defini mai multe astfel de reprezentari, precum si algoritmi
    care opereaza pe grafuri.
        Algoritmii vor fi independenti de reprezentarea interna a grafului - ei
    vor functiona indiferent de ce structura de date am ales noi pentru un anume graf.
        Pentru a obtine aceasta genericitate vom abstractiza notiunea de graf intr-o
    clasa care va expune operatiile pe care orice structura de date de tip graf ar
    trebui sa le aiba.

-}

-- reprezentam nodurile ca intregi
type Node = Int

-- reprezentam arcele ca perechi de noduri
type Arc = (Node, Node) 

-------------------------------------------------------------------------------

{-
    1.(1.5p) Analizati clasa Graph definita mai jos si scrieti implementarile default
    pentru functiile din aceasta clasa.
-}

-- Clasa Graph care defineste interfata pentru toate structurile de grafuri pe
-- care le vom defini mai jos.
class Graph g where
    
    -- Construieste un graf plecand de la o lista de noduri si arcele dintre noduri
    build :: [Node] -> [Arc] -> g

    -- Lista tuturor nodurilor din graf
    nodes :: g -> [Node] -- lista nodurilor din graf
    
    -- Lista arcelor din graf
    arcs :: g -> [Arc] -- lista muchiilor din graf

    -- lista nodurilor catre care nodul dat ca parametru are un arc
    nodeOut :: g -> Node -> [Node]

    -- lista nodurilor care au un arc catre nodul dat ca parametru
    nodeIn :: g -> Node -> [Node]
    
    -- verifica daca exista un arc intre 2 noduri
    arcExists :: g -> Node -> Node -> Bool

    -- TODO: implementari default
    arcExists g a b = elem (a, b) (arcs g)
    arcs g = [(a, b) | a <- (nodes g), b <- (nodes g), arcExists g a b]
    nodeIn g n = [a | a <- (nodes g), arcExists g a n]
    nodeOut g n = [a | a <- (nodes g), arcExists g n a]
-------------------------------------------------------------------------------
{-
    2.(2p) Definiti tipul AdjListGraph care reprezinta un graf ca pe o serie de
    perechi (nod, lista vecini). Includeti AdjListGraph in clasa Graph
-}
data AdjListGraph = Adj [(Node, [Node])]
instance Graph AdjListGraph where
    build n a = Adj [(x, [y | (z, y) <- a, z == x]) | x <- n]
    nodes (Adj g) = [x | (x, y) <- g]
    arcs (Adj g) = [(fst x, y) | x <- g, y <- snd x]
-------------------------------------------------------------------------------
{-
    3. (1p) Definiti tipul ArcGraph care reprezinta un graf ca doua lista separate:
    una cu nodurile din graf si una cu arcele din graf.
    Includeti ArcGraph in clasa Graph.
-}
data ArcGraph = ArcG [Node] [Arc]
instance Graph ArcGraph where
    build n a = ArcG n a
    nodes (ArcG n a) = n
    arcs (ArcG n a) = a
-------------------------------------------------------------------------------
{-
    4. (0.5p) Definiti functia convert care face conversia intre reprezentari diferite
    de grafuri.
-}
convert :: (Graph g1, Graph g2) => g1 -> g2
convert g1 = build (nodes g1) (arcs g1)
-------------------------------------------------------------------------------
{-
    O traversare a unui graf este o lista de perechi (nod, Maybe parinte-node). Aceasta
    structura va contine toate nodurile rezultate in urma unei parcurgeri a unui graf,
    impreuna cu parintele nodului din parcurgere (Pentru un nod N, parintele sau este
    nodul din care s-a ajuns la N in decursul parcurgerii)
-}
type Traversal = [(Node, Maybe Node)]

-- O cale in graf este reprezentata ca lista de noduri din acea cale.
type Path = [Node]

{-
    Definitie pentru algoritmi de parcurgere a unui graf. Un algoritm de parcurgere este o functie
    care primeste un graf si un nod de start si intoarce o structura Traversal. Observati
    ca tipul grafului este o variabila de tip - algoritmii trebuie sa functioneze pentru orice
    structura de graf.
-}
type TraverseAlgo g = g -> Node -> Traversal

-------------------------------------------------------------------------------

{-
    5. (2p) Implementati algoritmul de parcurgere in adancime. (Depth-First Search)
-}
bfs :: Graph g => TraverseAlgo g
bfs g start = myBfs g [(start, Nothing)] (map (\x -> (x, Just start)) (nodeOut g start))
    where
        myBfs graph visited queue
            | queue == [] = visited -- daca queue a ajuns vida, intorc visited
            | otherwise = myBfs g ((head queue) : visited) ((tail queue) ++ [(x, Just (fst (head queue))) | x <- (nodeOut g (fst (head queue))), not (elem x (map fst visited)), not (elem x (map fst (tail queue)))])
-- apelez recursiv pentru primul element din queue si reconstruiesc coada
-- la restul cozii adaug vecinii nevizitati ai nodului curent si care nu se afla deja in coada
--------------------

{-
    6. (2p) Implementati algoritmul de parcurgere in latime. (Breadth-First Search)
-}
dfs :: Graph g => TraverseAlgo g
dfs g start = myDfs g [(start, Nothing)] (map (\x -> (x, Just start)) (nodeOut g start))
    where
        myDfs graph visited stack
            | stack == [] = visited
            | otherwise = myDfs g ((head stack) : visited) ([(x, Just (fst (head stack))) | x <- (nodeOut g (fst (head stack))), not (elem x (map fst stack)), not (elem x (map fst (tail stack)))] ++ (tail stack))
--acelasi rationament, dar bazat pe stiva
-------------------------------------------------------------------------------

{-
    7. (1p) Definiti functia findPath care primeste ca parametri:
    * un algoritmi de parcurgere
    * un graf
    * un nod de pornire
    * un nod destinatie
    si care intoarce o cale dintre cele 2 noduri, daca o astfel de cale
    exista.
-}

getNext :: Traversal -> Path -> Node -> Node -> Path
getNext traversed p n1 n2 = 
    if n1 == n2
        then p
        else if length rez == 0
            then []
            else head rez
            where 
                rez = filter (\l -> not (null l))  (map (\(x, _) -> getNext traversed (p ++ [x]) x n2) (filter (\(_, x) -> Just n1 == x) traversed))



findPath :: Graph g => TraverseAlgo g -> g -> Node -> Node -> Maybe Path
findPath traverse g n1 n2 = 
    if r == [] then Nothing 
        else Just r where 
            r = getNext (traverse g n1) [n1] n1 n2

-------------------------------------------------------------------------------

{-
    8. (BONUS 0.5p). Creati tipul GenericAdjListGraph care este este similar cu
    AdjListGraph, dar in care nodurile nu sunt neaparat de tip Int - pot
    fi de orice tip.
-}


{-
    9. (BONUS 0.5p). Adaugati tipul GenericAdjListGraph la clasa Functor
-}

{-
    10. (BONUS 1p). Definiti clasa Listable pentru containere care contin elemente
    si care va avea o singura functie: toList - care intoarce lista elementelor
    continute in container. Adaugati GenericAdjListGraph la aceasta clasa.
-}